$OpenBSD: patch-lib_Target_PowerPC_InstPrinter_PPCInstPrinter_cpp,v 1.1 2014/09/11 17:54:13 brad Exp $

r213960
[PowerPC] Support TLS on PPC32/ELF

--- lib/Target/PowerPC/InstPrinter/PPCInstPrinter.cpp.orig	Sun Jul 27 03:53:55 2014
+++ lib/Target/PowerPC/InstPrinter/PPCInstPrinter.cpp	Sun Jul 27 03:56:03 2014
@@ -18,6 +18,7 @@
 #include "llvm/MC/MCExpr.h"
 #include "llvm/MC/MCInst.h"
 #include "llvm/MC/MCInstrInfo.h"
+#include "llvm/MC/MCSymbol.h"
 #include "llvm/Support/CommandLine.h"
 #include "llvm/Support/raw_ostream.h"
 #include "llvm/Target/TargetOpcodes.h"
@@ -300,10 +301,16 @@ void PPCInstPrinter::printMemRegReg(const MCInst *MI, 
 
 void PPCInstPrinter::printTLSCall(const MCInst *MI, unsigned OpNo,
                                   raw_ostream &O) {
-  printBranchOperand(MI, OpNo, O);
+  // On PPC64, VariantKind is VK_None, but on PPC32, it's VK_PLT, and it must
+  // come at the _end_ of the expression.
+  const MCOperand &Op = MI->getOperand(OpNo);
+  const MCSymbolRefExpr &refExp = cast<MCSymbolRefExpr>(*Op.getExpr());
+  O << refExp.getSymbol().getName();
   O << '(';
   printOperand(MI, OpNo+1, O);
   O << ')';
+  if (refExp.getKind() != MCSymbolRefExpr::VK_None)
+    O << '@' << MCSymbolRefExpr::getVariantKindName(refExp.getKind());
 }
 
 
