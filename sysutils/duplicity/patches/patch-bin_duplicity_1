$OpenBSD: patch-bin_duplicity_1,v 1.3 2016/01/17 05:30:31 shadchin Exp $
Document the ssh backend change from paramiko to pexpect.
--- bin/duplicity.1.orig	Mon Dec  7 12:03:39 2015
+++ bin/duplicity.1	Thu Dec 24 19:56:18 2015
@@ -432,7 +432,7 @@ should be given relative to the root of the directory 
 
 
 .TP
-.BI "--file-prefix, --file-prefix-manifest, --file-prefix-archive, --file-prefix-signature
+.BI "--file-prefix, --file-prefix-manifest, --file-prefix-archive, --file-prefix-signature"
 Adds a prefix to all files, manifest files, archive files, and/or signature files.
 
 The same set of prefixes must be passed in on backup and restore.
@@ -850,7 +850,6 @@ currently supports only the
 or
 .B -oIdentityFile
 setting. If needed provide more host specific options via ssh_config file.
-.RE
 
 .TP
 .BI "--ssl-cacert-file " file
@@ -1151,9 +1150,9 @@ scp://.. or
 .br
 sftp://user[:password]@other.host[:port]/[relative|/absolute]_path
 .PP
-.BR "defaults" " are paramiko+scp:// and paramiko+sftp://"
+.BR "defaults" " are pexpect+scp://, pexpect+sftp://, lftp+sftp://"
 .br
-.BR "alternatively" " try pexpect+scp://, pexpect+sftp://, lftp+sftp://"
+.BR "alternatively" " try paramiko+scp:// and paramiko+sftp://"
 .br
 See also
 .BR --ssh-askpass ,
@@ -1471,7 +1470,7 @@ which aren't followed by 'foo'.  However, it wouldn't 
 if /home/ben/1234567 existed.
 
 .SH A NOTE ON AZURE ACCESS
-The Azure backend requires the Microsoft Azure Storage SDK for Python to be 
+The Azure backend requires the Microsoft Azure Storage SDK for Python to be
 installed on the system.
 See
 .B REQUIREMENTS
@@ -1764,7 +1763,7 @@ about the requirements for a server to support
 .I scp/ssh
 access.
 To make it even more confusing the user can choose between several ssh backends via a scheme prefix:
-paramiko+ (default), pexpect+, lftp+... .
+paramiko+, pexpect+ (default), lftp+... .
 .br
 paramiko & pexpect support
 .BR --use-scp ,
@@ -1776,7 +1775,7 @@ backend allows to define
 .BR --scp-command " and"
 .BR --sftp-command .
 .PP
-.BR "SSH paramiko backend " "(default)"
+.BR "SSH paramiko backend"
 is a complete reimplementation of ssh protocols natively in python. Advantages
 are speed and maintainability. Minor disadvantage is that extra packages are
 needed as listed in
@@ -1789,7 +1788,7 @@ mode (
 .I --use-scp
 ) though scp access is used for put/get operations but listing is done via ssh remote shell.
 .PP
-.B SSH pexpect backend
+.BR "SSH pexpect backend" " (selected by default on OpenBSD)"
 is the legacy ssh backend using the command line ssh binaries via pexpect.
 Older versions used
 .I scp
@@ -2050,7 +2049,7 @@ see pydrive backend
 (Python Cryptography Toolkit)
 - http://www.dlitz.net/software/pycrypto/
 .TP
-.B ssh pexpect backend
+.BR "ssh pexpect backend" " (default on OpenBSD)"
 .B sftp/scp client binaries
 OpenSSH - http://www.openssh.com/
 .br
