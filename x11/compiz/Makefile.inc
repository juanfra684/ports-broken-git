# $OpenBSD: Makefile.inc,v 1.2 2015/06/29 07:20:11 ajacoutot Exp $

PORTROACH +=	limitw:1,even

SHARED_ONLY =	Yes

CATEGORIES =	x11

HOMEPAGE =	http://www.compiz.org/

# MIT, GPL and LGPL
PERMIT_PACKAGE_CDROM =	Yes

DIST_SUBDIR =	compiz
