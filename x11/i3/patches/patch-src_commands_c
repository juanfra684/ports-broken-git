$OpenBSD: patch-src_commands_c,v 1.17 2016/01/26 09:42:35 dcoppa Exp $

Fix duplicated marks on append_layout.

Correctly handle command criteria for "move window to output".

Migrate the resize command to use typed numbers.

Migrate the move command to use typed numbers.

Turn "char *" into "const char *" for all command parser functions.

Fix crash when trying to split and float a dock container.

Refactor parsing of matches to avoid code duplication.

Remove commented-out code.

--- src/commands.c.orig	Wed Sep 30 08:55:10 2015
+++ src/commands.c	Tue Jan 26 10:20:25 2016
@@ -83,7 +83,7 @@ static Output *get_output_of_con(Con *con) {
  * and return true, signaling that no further workspace switching should occur in the calling function.
  *
  */
-static bool maybe_back_and_forth(struct CommandResultIR *cmd_output, char *name) {
+static bool maybe_back_and_forth(struct CommandResultIR *cmd_output, const char *name) {
     Con *ws = con_get_workspace(focused);
 
     /* If we switched to a different workspace, do nothing */
@@ -121,102 +121,6 @@ static Con *maybe_auto_back_and_forth_workspace(Con *w
     return workspace;
 }
 
-// This code is commented out because we might recycle it for popping up error
-// messages on parser errors.
-#if 0
-static pid_t migration_pid = -1;
-
-/*
- * Handler which will be called when we get a SIGCHLD for the nagbar, meaning
- * it exited (or could not be started, depending on the exit code).
- *
- */
-static void nagbar_exited(EV_P_ ev_child *watcher, int revents) {
-    ev_child_stop(EV_A_ watcher);
-    if (!WIFEXITED(watcher->rstatus)) {
-        fprintf(stderr, "ERROR: i3-nagbar did not exit normally.\n");
-        return;
-    }
-
-    int exitcode = WEXITSTATUS(watcher->rstatus);
-    printf("i3-nagbar process exited with status %d\n", exitcode);
-    if (exitcode == 2) {
-        fprintf(stderr, "ERROR: i3-nagbar could not be found. Is it correctly installed on your system?\n");
-    }
-
-    migration_pid = -1;
-}
-
-/* We need ev >= 4 for the following code. Since it is not *that* important (it
- * only makes sure that there are no i3-nagbar instances left behind) we still
- * support old systems with libev 3. */
-#if EV_VERSION_MAJOR >= 4
-/*
- * Cleanup handler. Will be called when i3 exits. Kills i3-nagbar with signal
- * SIGKILL (9) to make sure there are no left-over i3-nagbar processes.
- *
- */
-static void nagbar_cleanup(EV_P_ ev_cleanup *watcher, int revent) {
-    if (migration_pid != -1) {
-        LOG("Sending SIGKILL (9) to i3-nagbar with PID %d\n", migration_pid);
-        kill(migration_pid, SIGKILL);
-    }
-}
-#endif
-
-void cmd_MIGRATION_start_nagbar(void) {
-    if (migration_pid != -1) {
-        fprintf(stderr, "i3-nagbar already running.\n");
-        return;
-    }
-    fprintf(stderr, "Starting i3-nagbar, command parsing differs from expected output.\n");
-    ELOG("Please report this on IRC or in the bugtracker. Make sure to include the full debug level logfile:\n");
-    ELOG("i3-dump-log | gzip -9c > /tmp/i3.log.gz\n");
-    ELOG("FYI: Your i3 version is " I3_VERSION "\n");
-    migration_pid = fork();
-    if (migration_pid == -1) {
-        warn("Could not fork()");
-        return;
-    }
-
-    /* child */
-    if (migration_pid == 0) {
-        char *pageraction;
-        sasprintf(&pageraction, "i3-sensible-terminal -e i3-sensible-pager \"%s\"", errorfilename);
-        char *argv[] = {
-            NULL, /* will be replaced by the executable path */
-            "-t",
-            "error",
-            "-m",
-            "You found a parsing error. Please, please, please, report it!",
-            "-b",
-            "show errors",
-            pageraction,
-            NULL
-        };
-        exec_i3_utility("i3-nagbar", argv);
-    }
-
-    /* parent */
-    /* install a child watcher */
-    ev_child *child = smalloc(sizeof(ev_child));
-    ev_child_init(child, &nagbar_exited, migration_pid, 0);
-    ev_child_start(main_loop, child);
-
-/* We need ev >= 4 for the following code. Since it is not *that* important (it
- * only makes sure that there are no i3-nagbar instances left behind) we still
- * support old systems with libev 3. */
-#if EV_VERSION_MAJOR >= 4
-    /* install a cleanup watcher (will be called when i3 exits and i3-nagbar is
-     * still running) */
-    ev_cleanup *cleanup = smalloc(sizeof(ev_cleanup));
-    ev_cleanup_init(cleanup, nagbar_cleanup);
-    ev_cleanup_start(main_loop, cleanup);
-#endif
-}
-
-#endif
-
 /*******************************************************************************
  * Criteria functions.
  ******************************************************************************/
@@ -315,108 +219,8 @@ void cmd_criteria_match_windows(I3_CMD) {
  * specification.
  *
  */
-void cmd_criteria_add(I3_CMD, char *ctype, char *cvalue) {
-    DLOG("ctype=*%s*, cvalue=*%s*\n", ctype, cvalue);
-
-    if (strcmp(ctype, "class") == 0) {
-        current_match->class = regex_new(cvalue);
-        return;
-    }
-
-    if (strcmp(ctype, "instance") == 0) {
-        current_match->instance = regex_new(cvalue);
-        return;
-    }
-
-    if (strcmp(ctype, "window_role") == 0) {
-        current_match->window_role = regex_new(cvalue);
-        return;
-    }
-
-    if (strcmp(ctype, "con_id") == 0) {
-        char *end;
-        long parsed = strtol(cvalue, &end, 0);
-        if (parsed == LONG_MIN ||
-            parsed == LONG_MAX ||
-            parsed < 0 ||
-            (end && *end != '\0')) {
-            ELOG("Could not parse con id \"%s\"\n", cvalue);
-        } else {
-            current_match->con_id = (Con *)parsed;
-            DLOG("id as int = %p\n", current_match->con_id);
-        }
-        return;
-    }
-
-    if (strcmp(ctype, "id") == 0) {
-        char *end;
-        long parsed = strtol(cvalue, &end, 0);
-        if (parsed == LONG_MIN ||
-            parsed == LONG_MAX ||
-            parsed < 0 ||
-            (end && *end != '\0')) {
-            ELOG("Could not parse window id \"%s\"\n", cvalue);
-        } else {
-            current_match->id = parsed;
-            DLOG("window id as int = %d\n", current_match->id);
-        }
-        return;
-    }
-
-    if (strcmp(ctype, "window_type") == 0) {
-        if (strcasecmp(cvalue, "normal") == 0)
-            current_match->window_type = A__NET_WM_WINDOW_TYPE_NORMAL;
-        else if (strcasecmp(cvalue, "dialog") == 0)
-            current_match->window_type = A__NET_WM_WINDOW_TYPE_DIALOG;
-        else if (strcasecmp(cvalue, "utility") == 0)
-            current_match->window_type = A__NET_WM_WINDOW_TYPE_UTILITY;
-        else if (strcasecmp(cvalue, "toolbar") == 0)
-            current_match->window_type = A__NET_WM_WINDOW_TYPE_TOOLBAR;
-        else if (strcasecmp(cvalue, "splash") == 0)
-            current_match->window_type = A__NET_WM_WINDOW_TYPE_SPLASH;
-        else if (strcasecmp(cvalue, "menu") == 0)
-            current_match->window_type = A__NET_WM_WINDOW_TYPE_MENU;
-        else if (strcasecmp(cvalue, "dropdown_menu") == 0)
-            current_match->window_type = A__NET_WM_WINDOW_TYPE_DROPDOWN_MENU;
-        else if (strcasecmp(cvalue, "popup_menu") == 0)
-            current_match->window_type = A__NET_WM_WINDOW_TYPE_POPUP_MENU;
-        else if (strcasecmp(cvalue, "tooltip") == 0)
-            current_match->window_type = A__NET_WM_WINDOW_TYPE_TOOLTIP;
-        else
-            ELOG("unknown window_type value \"%s\"\n", cvalue);
-
-        return;
-    }
-
-    if (strcmp(ctype, "con_mark") == 0) {
-        current_match->mark = regex_new(cvalue);
-        return;
-    }
-
-    if (strcmp(ctype, "title") == 0) {
-        current_match->title = regex_new(cvalue);
-        return;
-    }
-
-    if (strcmp(ctype, "urgent") == 0) {
-        if (strcasecmp(cvalue, "latest") == 0 ||
-            strcasecmp(cvalue, "newest") == 0 ||
-            strcasecmp(cvalue, "recent") == 0 ||
-            strcasecmp(cvalue, "last") == 0) {
-            current_match->urgent = U_LATEST;
-        } else if (strcasecmp(cvalue, "oldest") == 0 ||
-                   strcasecmp(cvalue, "first") == 0) {
-            current_match->urgent = U_OLDEST;
-        }
-        return;
-    }
-
-    if (strcmp(ctype, "workspace") == 0) {
-        current_match->workspace = regex_new(cvalue);
-        return;
-    }
-
-    ELOG("Unknown criterion: %s\n", ctype);
+void cmd_criteria_add(I3_CMD, const char *ctype, const char *cvalue) {
+    match_parse_property(current_match, ctype, cvalue);
 }
 
 /*
@@ -424,7 +228,7 @@ void cmd_criteria_add(I3_CMD, char *ctype, char *cvalu
  * next|prev|next_on_output|prev_on_output|current'.
  *
  */
-void cmd_move_con_to_workspace(I3_CMD, char *which) {
+void cmd_move_con_to_workspace(I3_CMD, const char *which) {
     owindow *current;
 
     DLOG("which=%s\n", which);
@@ -500,7 +304,7 @@ void cmd_move_con_to_workspace_back_and_forth(I3_CMD) 
  * Implementation of 'move [window|container] [to] workspace <name>'.
  *
  */
-void cmd_move_con_to_workspace_name(I3_CMD, char *name) {
+void cmd_move_con_to_workspace_name(I3_CMD, const char *name) {
     if (strncasecmp(name, "__", strlen("__")) == 0) {
         LOG("You cannot move containers to i3-internal workspaces (\"%s\").\n", name);
         ysuccess(false);
@@ -544,7 +348,7 @@ void cmd_move_con_to_workspace_name(I3_CMD, char *name
  * Implementation of 'move [window|container] [to] workspace number <name>'.
  *
  */
-void cmd_move_con_to_workspace_number(I3_CMD, char *which) {
+void cmd_move_con_to_workspace_number(I3_CMD, const char *which) {
     owindow *current;
 
     /* We have nothing to move:
@@ -591,7 +395,7 @@ void cmd_move_con_to_workspace_number(I3_CMD, char *wh
     ysuccess(true);
 }
 
-static void cmd_resize_floating(I3_CMD, char *way, char *direction, Con *floating_con, int px) {
+static void cmd_resize_floating(I3_CMD, const char *way, const char *direction, Con *floating_con, int px) {
     LOG("floating resize\n");
     Rect old_rect = floating_con->rect;
     Con *focused_con = con_descend_focused(floating_con);
@@ -643,7 +447,7 @@ static void cmd_resize_floating(I3_CMD, char *way, cha
         floating_con->scratchpad_state = SCRATCHPAD_CHANGED;
 }
 
-static bool cmd_resize_tiling_direction(I3_CMD, Con *current, char *way, char *direction, int ppt) {
+static bool cmd_resize_tiling_direction(I3_CMD, Con *current, const char *way, const char *direction, int ppt) {
     LOG("tiling resize\n");
     Con *second = NULL;
     Con *first = current;
@@ -696,7 +500,7 @@ static bool cmd_resize_tiling_direction(I3_CMD, Con *c
     return true;
 }
 
-static bool cmd_resize_tiling_width_height(I3_CMD, Con *current, char *way, char *direction, int ppt) {
+static bool cmd_resize_tiling_width_height(I3_CMD, Con *current, const char *way, const char *direction, int ppt) {
     LOG("width/height resize\n");
     /* get the appropriate current container (skip stacked/tabbed cons) */
     while (current->parent->layout == L_STACKED ||
@@ -782,15 +586,11 @@ static bool cmd_resize_tiling_width_height(I3_CMD, Con
  * Implementation of 'resize grow|shrink <direction> [<px> px] [or <ppt> ppt]'.
  *
  */
-void cmd_resize(I3_CMD, char *way, char *direction, char *resize_px, char *resize_ppt) {
-    /* resize <grow|shrink> <direction> [<px> px] [or <ppt> ppt] */
-    DLOG("resizing in way %s, direction %s, px %s or ppt %s\n", way, direction, resize_px, resize_ppt);
-    // TODO: We could either handle this in the parser itself as a separate token (and make the stack typed) or we need a better way to convert a string to a number with error checking
-    int px = atoi(resize_px);
-    int ppt = atoi(resize_ppt);
+void cmd_resize(I3_CMD, const char *way, const char *direction, long resize_px, long resize_ppt) {
+    DLOG("resizing in way %s, direction %s, px %ld or ppt %ld\n", way, direction, resize_px, resize_ppt);
     if (strcmp(way, "shrink") == 0) {
-        px *= -1;
-        ppt *= -1;
+        resize_px *= -1;
+        resize_ppt *= -1;
     }
 
     HANDLE_EMPTY_MATCH;
@@ -805,14 +605,16 @@ void cmd_resize(I3_CMD, char *way, char *direction, ch
 
         Con *floating_con;
         if ((floating_con = con_inside_floating(current->con))) {
-            cmd_resize_floating(current_match, cmd_output, way, direction, floating_con, px);
+            cmd_resize_floating(current_match, cmd_output, way, direction, floating_con, resize_px);
         } else {
             if (strcmp(direction, "width") == 0 ||
                 strcmp(direction, "height") == 0) {
-                if (!cmd_resize_tiling_width_height(current_match, cmd_output, current->con, way, direction, ppt))
+                if (!cmd_resize_tiling_width_height(current_match, cmd_output,
+                                                    current->con, way, direction, resize_ppt))
                     return;
             } else {
-                if (!cmd_resize_tiling_direction(current_match, cmd_output, current->con, way, direction, ppt))
+                if (!cmd_resize_tiling_direction(current_match, cmd_output,
+                                                 current->con, way, direction, resize_ppt))
                     return;
             }
         }
@@ -827,13 +629,10 @@ void cmd_resize(I3_CMD, char *way, char *direction, ch
  * Implementation of 'resize set <px> [px] <px> [px]'.
  *
  */
-void cmd_size(I3_CMD, char *cwidth, char *cheight) {
-    DLOG("resizing to %sx%s px\n", cwidth, cheight);
-    // TODO: We could either handle this in the parser itself as a separate token (and make the stack typed) or we need a better way to convert a string to a number with error checking
-    int x = atoi(cwidth);
-    int y = atoi(cheight);
-    if (x <= 0 || y <= 0) {
-        ELOG("Resize failed: dimensions cannot be negative (was %sx%s)\n", cwidth, cheight);
+void cmd_resize_set(I3_CMD, long cwidth, long cheight) {
+    DLOG("resizing to %ldx%ld px\n", cwidth, cheight);
+    if (cwidth <= 0 || cheight <= 0) {
+        ELOG("Resize failed: dimensions cannot be negative (was %ldx%ld)\n", cwidth, cheight);
         return;
     }
 
@@ -843,7 +642,7 @@ void cmd_size(I3_CMD, char *cwidth, char *cheight) {
     TAILQ_FOREACH(current, &owindows, owindows) {
         Con *floating_con;
         if ((floating_con = con_inside_floating(current->con))) {
-            floating_resize(floating_con, x, y);
+            floating_resize(floating_con, cwidth, cheight);
         } else {
             ELOG("Resize failed: %p not a floating container\n", current->con);
         }
@@ -858,7 +657,7 @@ void cmd_size(I3_CMD, char *cwidth, char *cheight) {
  * Implementation of 'border normal|pixel [<n>]', 'border none|1pixel|toggle'.
  *
  */
-void cmd_border(I3_CMD, char *border_style_str, char *border_width) {
+void cmd_border(I3_CMD, const char *border_style_str, const char *border_width) {
     DLOG("border style should be changed to %s with border width %s\n", border_style_str, border_width);
     owindow *current;
 
@@ -911,7 +710,7 @@ void cmd_border(I3_CMD, char *border_style_str, char *
  * Implementation of 'nop <comment>'.
  *
  */
-void cmd_nop(I3_CMD, char *comment) {
+void cmd_nop(I3_CMD, const char *comment) {
     LOG("-------------------------------------------------\n");
     LOG("  NOP: %s\n", comment);
     LOG("-------------------------------------------------\n");
@@ -921,7 +720,8 @@ void cmd_nop(I3_CMD, char *comment) {
  * Implementation of 'append_layout <path>'.
  *
  */
-void cmd_append_layout(I3_CMD, char *path) {
+void cmd_append_layout(I3_CMD, const char *cpath) {
+    char *path = sstrdup(cpath);
     LOG("Appending layout \"%s\"\n", path);
 
     /* Make sure we allow paths like '~/.i3/layout.json' */
@@ -982,7 +782,7 @@ void cmd_append_layout(I3_CMD, char *path) {
  * Implementation of 'workspace next|prev|next_on_output|prev_on_output'.
  *
  */
-void cmd_workspace(I3_CMD, char *which) {
+void cmd_workspace(I3_CMD, const char *which) {
     Con *ws;
 
     DLOG("which=%s\n", which);
@@ -1018,7 +818,7 @@ void cmd_workspace(I3_CMD, char *which) {
  * Implementation of 'workspace number <name>'
  *
  */
-void cmd_workspace_number(I3_CMD, char *which) {
+void cmd_workspace_number(I3_CMD, const char *which) {
     Con *output, *workspace = NULL;
 
     if (con_get_fullscreen_con(croot, CF_GLOBAL)) {
@@ -1077,7 +877,7 @@ void cmd_workspace_back_and_forth(I3_CMD) {
  * Implementation of 'workspace <name>'
  *
  */
-void cmd_workspace_name(I3_CMD, char *name) {
+void cmd_workspace_name(I3_CMD, const char *name) {
     if (strncasecmp(name, "__", strlen("__")) == 0) {
         LOG("You cannot switch to the i3-internal workspaces (\"%s\").\n", name);
         ysuccess(false);
@@ -1104,7 +904,7 @@ void cmd_workspace_name(I3_CMD, char *name) {
  * Implementation of 'mark [--toggle] <mark>'
  *
  */
-void cmd_mark(I3_CMD, char *mark, char *toggle) {
+void cmd_mark(I3_CMD, const char *mark, const char *toggle) {
     HANDLE_EMPTY_MATCH;
 
     owindow *current = TAILQ_FIRST(&owindows);
@@ -1135,7 +935,7 @@ void cmd_mark(I3_CMD, char *mark, char *toggle) {
  * Implementation of 'unmark [mark]'
  *
  */
-void cmd_unmark(I3_CMD, char *mark) {
+void cmd_unmark(I3_CMD, const char *mark) {
     con_unmark(mark);
 
     cmd_output->needs_tree_render = true;
@@ -1147,7 +947,7 @@ void cmd_unmark(I3_CMD, char *mark) {
  * Implementation of 'mode <string>'.
  *
  */
-void cmd_mode(I3_CMD, char *mode) {
+void cmd_mode(I3_CMD, const char *mode) {
     DLOG("mode=%s\n", mode);
     switch_mode(mode);
 
@@ -1159,7 +959,7 @@ void cmd_mode(I3_CMD, char *mode) {
  * Implementation of 'move [window|container] [to] output <str>'.
  *
  */
-void cmd_move_con_to_output(I3_CMD, char *name) {
+void cmd_move_con_to_output(I3_CMD, const char *name) {
     DLOG("Should move window to output \"%s\".\n", name);
     HANDLE_EMPTY_MATCH;
 
@@ -1197,7 +997,7 @@ void cmd_move_con_to_output(I3_CMD, char *name) {
  * Implementation of 'move [container|window] [to] mark <str>'.
  *
  */
-void cmd_move_con_to_mark(I3_CMD, char *mark) {
+void cmd_move_con_to_mark(I3_CMD, const char *mark) {
     DLOG("moving window to mark \"%s\"\n", mark);
 
     HANDLE_EMPTY_MATCH;
@@ -1217,7 +1017,7 @@ void cmd_move_con_to_mark(I3_CMD, char *mark) {
  * Implementation of 'floating enable|disable|toggle'
  *
  */
-void cmd_floating(I3_CMD, char *floating_mode) {
+void cmd_floating(I3_CMD, const char *floating_mode) {
     owindow *current;
 
     DLOG("floating_mode=%s\n", floating_mode);
@@ -1248,7 +1048,7 @@ void cmd_floating(I3_CMD, char *floating_mode) {
  * Implementation of 'move workspace to [output] <str>'.
  *
  */
-void cmd_move_workspace_to_output(I3_CMD, char *name) {
+void cmd_move_workspace_to_output(I3_CMD, const char *name) {
     DLOG("should move workspace to output %s\n", name);
 
     HANDLE_EMPTY_MATCH;
@@ -1273,17 +1073,19 @@ void cmd_move_workspace_to_output(I3_CMD, char *name) 
  * Implementation of 'split v|h|vertical|horizontal'.
  *
  */
-void cmd_split(I3_CMD, char *direction) {
+void cmd_split(I3_CMD, const char *direction) {
+    HANDLE_EMPTY_MATCH;
+
     owindow *current;
-    /* TODO: use matches */
     LOG("splitting in direction %c\n", direction[0]);
-    if (match_is_empty(current_match))
-        tree_split(focused, (direction[0] == 'v' ? VERT : HORIZ));
-    else {
-        TAILQ_FOREACH(current, &owindows, owindows) {
-            DLOG("matching: %p / %s\n", current->con, current->con->name);
-            tree_split(current->con, (direction[0] == 'v' ? VERT : HORIZ));
+    TAILQ_FOREACH(current, &owindows, owindows) {
+        if (con_is_docked(current->con)) {
+            ELOG("Cannot split a docked container, skipping.\n");
+            continue;
         }
+
+        DLOG("matching: %p / %s\n", current->con, current->con->name);
+        tree_split(current->con, (direction[0] == 'v' ? VERT : HORIZ));
     }
 
     cmd_output->needs_tree_render = true;
@@ -1295,7 +1097,7 @@ void cmd_split(I3_CMD, char *direction) {
  * Implementation of 'kill [window|client]'.
  *
  */
-void cmd_kill(I3_CMD, char *kill_mode_str) {
+void cmd_kill(I3_CMD, const char *kill_mode_str) {
     if (kill_mode_str == NULL)
         kill_mode_str = "window";
     owindow *current;
@@ -1332,7 +1134,7 @@ void cmd_kill(I3_CMD, char *kill_mode_str) {
  * Implementation of 'exec [--no-startup-id] <command>'.
  *
  */
-void cmd_exec(I3_CMD, char *nosn, char *command) {
+void cmd_exec(I3_CMD, const char *nosn, const char *command) {
     bool no_startup_id = (nosn != NULL);
 
     DLOG("should execute %s, no_startup_id = %d\n", command, no_startup_id);
@@ -1346,7 +1148,7 @@ void cmd_exec(I3_CMD, char *nosn, char *command) {
  * Implementation of 'focus left|right|up|down'.
  *
  */
-void cmd_focus_direction(I3_CMD, char *direction) {
+void cmd_focus_direction(I3_CMD, const char *direction) {
     DLOG("direction = *%s*\n", direction);
 
     if (strcmp(direction, "left") == 0)
@@ -1372,7 +1174,7 @@ void cmd_focus_direction(I3_CMD, char *direction) {
  * Implementation of 'focus tiling|floating|mode_toggle'.
  *
  */
-void cmd_focus_window_mode(I3_CMD, char *window_mode) {
+void cmd_focus_window_mode(I3_CMD, const char *window_mode) {
     DLOG("window_mode = %s\n", window_mode);
 
     Con *ws = con_get_workspace(focused);
@@ -1403,7 +1205,7 @@ void cmd_focus_window_mode(I3_CMD, char *window_mode) 
  * Implementation of 'focus parent|child'.
  *
  */
-void cmd_focus_level(I3_CMD, char *level) {
+void cmd_focus_level(I3_CMD, const char *level) {
     DLOG("level = %s\n", level);
     bool success = false;
 
@@ -1507,7 +1309,7 @@ void cmd_focus(I3_CMD) {
  *                   'fullscreen disable'
  *
  */
-void cmd_fullscreen(I3_CMD, char *action, char *fullscreen_mode) {
+void cmd_fullscreen(I3_CMD, const char *action, const char *fullscreen_mode) {
     fullscreen_mode_t mode = strcmp(fullscreen_mode, "global") == 0 ? CF_GLOBAL : CF_OUTPUT;
     DLOG("%s fullscreen, mode = %s\n", action, fullscreen_mode);
     owindow *current;
@@ -1534,7 +1336,7 @@ void cmd_fullscreen(I3_CMD, char *action, char *fullsc
  * Implementation of 'sticky enable|disable|toggle'.
  *
  */
-void cmd_sticky(I3_CMD, char *action) {
+void cmd_sticky(I3_CMD, const char *action) {
     DLOG("%s sticky on window\n", action);
     HANDLE_EMPTY_MATCH;
 
@@ -1570,28 +1372,25 @@ void cmd_sticky(I3_CMD, char *action) {
  * Implementation of 'move <direction> [<pixels> [px]]'.
  *
  */
-void cmd_move_direction(I3_CMD, char *direction, char *move_px) {
-    // TODO: We could either handle this in the parser itself as a separate token (and make the stack typed) or we need a better way to convert a string to a number with error checking
-    int px = atoi(move_px);
-
+void cmd_move_direction(I3_CMD, const char *direction, long move_px) {
     owindow *current;
     HANDLE_EMPTY_MATCH;
 
     Con *initially_focused = focused;
 
     TAILQ_FOREACH(current, &owindows, owindows) {
-        DLOG("moving in direction %s, px %s\n", direction, move_px);
+        DLOG("moving in direction %s, px %ld\n", direction, move_px);
         if (con_is_floating(current->con)) {
-            DLOG("floating move with %d pixels\n", px);
+            DLOG("floating move with %ld pixels\n", move_px);
             Rect newrect = current->con->parent->rect;
             if (strcmp(direction, "left") == 0) {
-                newrect.x -= px;
+                newrect.x -= move_px;
             } else if (strcmp(direction, "right") == 0) {
-                newrect.x += px;
+                newrect.x += move_px;
             } else if (strcmp(direction, "up") == 0) {
-                newrect.y -= px;
+                newrect.y -= move_px;
             } else if (strcmp(direction, "down") == 0) {
-                newrect.y += px;
+                newrect.y += move_px;
             }
             floating_reposition(current->con->parent, newrect);
         } else {
@@ -1612,10 +1411,11 @@ void cmd_move_direction(I3_CMD, char *direction, char 
  * Implementation of 'layout default|stacked|stacking|tabbed|splitv|splith'.
  *
  */
-void cmd_layout(I3_CMD, char *layout_str) {
+void cmd_layout(I3_CMD, const char *layout_str) {
+    HANDLE_EMPTY_MATCH;
+
     if (strcmp(layout_str, "stacking") == 0)
         layout_str = "stacked";
-    owindow *current;
     layout_t layout;
     /* default is a special case which will be handled in con_set_layout(). */
     if (strcmp(layout_str, "default") == 0)
@@ -1635,14 +1435,15 @@ void cmd_layout(I3_CMD, char *layout_str) {
 
     DLOG("changing layout to %s (%d)\n", layout_str, layout);
 
-    /* check if the match is empty, not if the result is empty */
-    if (match_is_empty(current_match))
-        con_set_layout(focused, layout);
-    else {
-        TAILQ_FOREACH(current, &owindows, owindows) {
-            DLOG("matching: %p / %s\n", current->con, current->con->name);
-            con_set_layout(current->con, layout);
+    owindow *current;
+    TAILQ_FOREACH(current, &owindows, owindows) {
+        if (con_is_docked(current->con)) {
+            ELOG("cannot change layout of a docked container, skipping it.\n");
+            continue;
         }
+
+        DLOG("matching: %p / %s\n", current->con, current->con->name);
+        con_set_layout(current->con, layout);
     }
 
     cmd_output->needs_tree_render = true;
@@ -1654,7 +1455,7 @@ void cmd_layout(I3_CMD, char *layout_str) {
  * Implementation of 'layout toggle [all|split]'.
  *
  */
-void cmd_layout_toggle(I3_CMD, char *toggle_mode) {
+void cmd_layout_toggle(I3_CMD, const char *toggle_mode) {
     owindow *current;
 
     if (toggle_mode == NULL)
@@ -1751,7 +1552,7 @@ void cmd_open(I3_CMD) {
  * Implementation of 'focus output <output>'.
  *
  */
-void cmd_focus_output(I3_CMD, char *name) {
+void cmd_focus_output(I3_CMD, const char *name) {
     owindow *current;
 
     DLOG("name = %s\n", name);
@@ -1793,9 +1594,7 @@ void cmd_focus_output(I3_CMD, char *name) {
  * Implementation of 'move [window|container] [to] [absolute] position <px> [px] <px> [px]
  *
  */
-void cmd_move_window_to_position(I3_CMD, char *method, char *cx, char *cy) {
-    int x = atoi(cx);
-    int y = atoi(cy);
+void cmd_move_window_to_position(I3_CMD, const char *method, long x, long y) {
     bool has_error = false;
 
     owindow *current;
@@ -1817,7 +1616,7 @@ void cmd_move_window_to_position(I3_CMD, char *method,
             current->con->parent->rect.x = x;
             current->con->parent->rect.y = y;
 
-            DLOG("moving to absolute position %d %d\n", x, y);
+            DLOG("moving to absolute position %ld %ld\n", x, y);
             floating_maybe_reassign_ws(current->con->parent);
             cmd_output->needs_tree_render = true;
         }
@@ -1825,7 +1624,7 @@ void cmd_move_window_to_position(I3_CMD, char *method,
         if (strcmp(method, "position") == 0) {
             Rect newrect = current->con->parent->rect;
 
-            DLOG("moving to position %d %d\n", x, y);
+            DLOG("moving to position %ld %ld\n", x, y);
             newrect.x = x;
             newrect.y = y;
 
@@ -1842,7 +1641,7 @@ void cmd_move_window_to_position(I3_CMD, char *method,
  * Implementation of 'move [window|container] [to] [absolute] position center
  *
  */
-void cmd_move_window_to_center(I3_CMD, char *method) {
+void cmd_move_window_to_center(I3_CMD, const char *method) {
     if (!con_is_floating(focused)) {
         ELOG("Cannot change position. The window/container is not floating\n");
         yerror("Cannot change position. The window/container is not floating.");
@@ -1938,7 +1737,7 @@ void cmd_scratchpad_show(I3_CMD) {
  * Implementation of 'title_format <format>'
  *
  */
-void cmd_title_format(I3_CMD, char *format) {
+void cmd_title_format(I3_CMD, const char *format) {
     DLOG("setting title_format to \"%s\"\n", format);
     HANDLE_EMPTY_MATCH;
 
@@ -1975,7 +1774,7 @@ void cmd_title_format(I3_CMD, char *format) {
  * Implementation of 'rename workspace [<name>] to <name>'
  *
  */
-void cmd_rename_workspace(I3_CMD, char *old_name, char *new_name) {
+void cmd_rename_workspace(I3_CMD, const char *old_name, const char *new_name) {
     if (strncasecmp(new_name, "__", strlen("__")) == 0) {
         LOG("Cannot rename workspace to \"%s\": names starting with __ are i3-internal.\n", new_name);
         ysuccess(false);
@@ -2060,7 +1859,7 @@ void cmd_rename_workspace(I3_CMD, char *old_name, char
  * Implementation of 'bar mode dock|hide|invisible|toggle [<bar_id>]'
  *
  */
-bool cmd_bar_mode(char *bar_mode, char *bar_id) {
+bool cmd_bar_mode(const char *bar_mode, const char *bar_id) {
     int mode = M_DOCK;
     bool toggle = false;
     if (strcmp(bar_mode, "dock") == 0)
@@ -2105,7 +1904,7 @@ bool cmd_bar_mode(char *bar_mode, char *bar_id) {
  * Implementation of 'bar hidden_state hide|show|toggle [<bar_id>]'
  *
  */
-bool cmd_bar_hidden_state(char *bar_hidden_state, char *bar_id) {
+bool cmd_bar_hidden_state(const char *bar_hidden_state, const char *bar_id) {
     int hidden_state = S_SHOW;
     bool toggle = false;
     if (strcmp(bar_hidden_state, "hide") == 0)
@@ -2148,7 +1947,7 @@ bool cmd_bar_hidden_state(char *bar_hidden_state, char
  * Implementation of 'bar (hidden_state hide|show|toggle)|(mode dock|hide|invisible|toggle) [<bar_id>]'
  *
  */
-void cmd_bar(I3_CMD, char *bar_type, char *bar_value, char *bar_id) {
+void cmd_bar(I3_CMD, const char *bar_type, const char *bar_value, const char *bar_id) {
     bool ret;
     if (strcmp(bar_type, "mode") == 0)
         ret = cmd_bar_mode(bar_value, bar_id);
@@ -2170,7 +1969,7 @@ void cmd_bar(I3_CMD, char *bar_type, char *bar_value, 
  * Implementation of 'shmlog <size>|toggle|on|off'
  *
  */
-void cmd_shmlog(I3_CMD, char *argument) {
+void cmd_shmlog(I3_CMD, const char *argument) {
     if (!strcmp(argument, "toggle"))
         /* Toggle shm log, if size is not 0. If it is 0, set it to default. */
         shmlog_size = shmlog_size ? -shmlog_size : default_shmlog_size;
@@ -2201,7 +2000,7 @@ void cmd_shmlog(I3_CMD, char *argument) {
  * Implementation of 'debuglog toggle|on|off'
  *
  */
-void cmd_debuglog(I3_CMD, char *argument) {
+void cmd_debuglog(I3_CMD, const char *argument) {
     bool logging = get_debug_logging();
     if (!strcmp(argument, "toggle")) {
         LOG("%s debug logging\n", logging ? "Disabling" : "Enabling");
