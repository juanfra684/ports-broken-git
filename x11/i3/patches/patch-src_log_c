$OpenBSD: patch-src_log_c,v 1.11 2016/01/18 10:04:29 tb Exp $

OpenBSD lacks pthread_condattr_setpshared()

--- src/log.c.orig	Wed Sep 30 08:55:10 2015
+++ src/log.c	Sun Jan 17 23:10:44 2016
@@ -20,7 +20,9 @@
 #include <sys/mman.h>
 #include <sys/stat.h>
 #include <errno.h>
+#if !defined(__OpenBSD__)
 #include <pthread.h>
+#endif
 
 #include "util.h"
 #include "log.h"
@@ -58,6 +60,8 @@ static char *loglastwrap;
 static int logbuffer_size;
 /* File descriptor for shm_open. */
 static int logbuffer_shm;
+/* Size (in bytes) of physical memory */
+static long long physical_mem_bytes;
 
 /*
  * Writes the offsets for the next write and for the last wrap to the
@@ -89,6 +93,16 @@ void init_logging(void) {
             }
         }
     }
+    if (physical_mem_bytes == 0) {
+#if defined(__APPLE__)
+        int mib[2] = {CTL_HW, HW_MEMSIZE};
+        size_t length = sizeof(long long);
+        sysctl(mib, 2, &physical_mem_bytes, &length, NULL, 0);
+#else
+        physical_mem_bytes = (long long)sysconf(_SC_PHYS_PAGES) *
+                             sysconf(_SC_PAGESIZE);
+#endif
+    }
     /* Start SHM logging if shmlog_size is > 0. shmlog_size is SHMLOG_SIZE by
      * default on development versions, and 0 on release versions. If it is
      * not > 0, the user has turned it off, so let's close the logbuffer. */
@@ -108,15 +122,6 @@ void open_logbuffer(void) {
          * For 512 MiB of RAM this will lead to a 5 MiB log buffer.
          * At the moment (2011-12-10), no testcase leads to an i3 log
          * of more than ~ 600 KiB. */
-    long long physical_mem_bytes;
-#if defined(__APPLE__)
-    int mib[2] = {CTL_HW, HW_MEMSIZE};
-    size_t length = sizeof(long long);
-    sysctl(mib, 2, &physical_mem_bytes, &length, NULL, 0);
-#else
-    physical_mem_bytes = (long long)sysconf(_SC_PHYS_PAGES) *
-                         sysconf(_SC_PAGESIZE);
-#endif
     logbuffer_size = min(physical_mem_bytes * 0.01, shmlog_size);
 #if defined(__FreeBSD__)
     sasprintf(&shmlogname, "/tmp/i3-log-%d", getpid());
@@ -154,11 +159,13 @@ void open_logbuffer(void) {
 
     header = (i3_shmlog_header *)logbuffer;
 
+#if !defined(__OpenBSD__)
     pthread_condattr_t cond_attr;
     pthread_condattr_init(&cond_attr);
     if (pthread_condattr_setpshared(&cond_attr, PTHREAD_PROCESS_SHARED) != 0)
         fprintf(stderr, "pthread_condattr_setpshared() failed, i3-dump-log -f will not work!\n");
     pthread_cond_init(&(header->condvar), &cond_attr);
+#endif
 
     logwalk = logbuffer + sizeof(i3_shmlog_header);
     loglastwrap = logbuffer + logbuffer_size;
@@ -273,8 +280,10 @@ static void vlog(const bool print, const char *fmt, va
 
         store_log_markers();
 
+#if !defined(__OpenBSD__)
         /* Wake up all (i3-dump-log) processes waiting for condvar. */
         pthread_cond_broadcast(&(header->condvar));
+#endif
 
         if (print)
             fwrite(message, len, 1, stdout);
