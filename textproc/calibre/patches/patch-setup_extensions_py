$OpenBSD: patch-setup_extensions_py,v 1.9 2015/12/15 22:03:59 sthen Exp $

- fix various header search paths
- don't override compiler optimizer levels
- build with libmtp/libusb1 on OpenBSD (possibly useless as it looks like it
also wants /sys/ access to actually detect devices..)

if updating patches, watch out for X11BASE -> /usr/X11R6 in last chunk,
and don't allow an updated patch to change the python shebang line,
it's correct with "env" here (handled in infrastructure).

--- setup/extensions.py.orig	Fri Dec 11 02:52:47 2015
+++ setup/extensions.py	Tue Dec 15 21:19:57 2015
@@ -9,7 +9,7 @@ __docformat__ = 'restructuredtext en'
 import textwrap, os, shlex, subprocess, glob, shutil, re, sys
 from distutils import sysconfig
 
-from setup import Command, islinux, isbsd, isosx, SRC, iswindows, __version__
+from setup import Command, islinux, isbsd, isosx, SRC, iswindows, isopenbsd, __version__
 from setup.build_environment import (
     chmlib_inc_dirs, podofo_inc, podofo_lib, podofo_error, pyqt, NMAKE, QMAKE,
     msvc, win_inc, win_lib, magick_inc_dirs, magick_lib_dirs, magick_libs,
@@ -45,13 +45,6 @@ class Extension(object):
         self.cflags = kwargs.get('cflags', [])
         self.ldflags = kwargs.get('ldflags', [])
         self.optional = kwargs.get('optional', False)
-        of = kwargs.get('optimize_level', None)
-        if of is None:
-            of = '/Ox' if iswindows else '-O3'
-        else:
-            flag = '/O%d' if iswindows else '-O%d'
-            of = flag % of
-        self.cflags.insert(0, of)
         self.qt_private_headers = kwargs.get('qt_private', [])
 
     def preflight(self, obj_dir, compiler, linker, builder, cflags, ldflags):
@@ -306,9 +299,10 @@ if isosx:
                 ldflags=['-framework', 'CoreServices', '-framework', 'IOKit'])
             )
 
-if islinux or isosx:
+if islinux or isosx or isopenbsd:
     extensions.append(Extension('libusb',
         ['calibre/devices/libusb/libusb.c'],
+        inc_dirs=[os.path.join(sysconfig.PREFIX, 'include')],
         libraries=['usb-1.0']
     ))
 
@@ -322,6 +316,7 @@ if islinux or isosx:
         'calibre/devices/mtp/unix/upstream/music-players.h',
         'calibre/devices/mtp/unix/upstream/device-flags.h',
         ],
+        inc_dirs=[os.path.join(sysconfig.PREFIX, 'include')],
         libraries=['mtp']
     ))
 
@@ -349,8 +344,8 @@ if isbsd:
     cflags.append('-pthread')
     ldflags.append('-shared')
     cflags.append('-I'+sysconfig.get_python_inc())
+    ldflags.append('-L'+os.path.join(sysconfig.PREFIX))
     ldflags.append('-lpython'+sysconfig.get_python_version())
-
 
 if isosx:
     cflags.append('-D_OSX')
