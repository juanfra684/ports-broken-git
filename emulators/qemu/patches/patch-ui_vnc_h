$OpenBSD: patch-ui_vnc_h,v 1.1 2015/08/11 21:28:53 sthen Exp $

CVE-2015-1779: incrementally decode websocket frames

The logic for decoding websocket frames wants to fully
decode the frame header and payload, before allowing the
VNC server to see any of the payload data. There is no
size limit on websocket payloads, so this allows a
malicious network client to consume 2^64 bytes in memory
in QEMU. It can trigger this denial of service before
the VNC server even performs any authentication.

The fix is to decode the header, and then incrementally
decode the payload data as it is needed. With this fix
the websocket decoder will allow at most 4k of data to
be buffered before decoding and processing payload.

--- ui/vnc.h.orig	Tue Mar 10 13:38:32 2015
+++ ui/vnc.h	Wed Aug  5 03:56:50 2015
@@ -302,6 +302,8 @@ struct VncState
 #ifdef CONFIG_VNC_WS
     Buffer ws_input;
     Buffer ws_output;
+    size_t ws_payload_remain;
+    WsMask ws_payload_mask;
 #endif
     /* current output mode information */
     VncWritePixels *write_pixels;
